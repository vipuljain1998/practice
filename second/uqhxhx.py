# -*- coding: utf-8 -*-
"""
Created on Sat May  9 14:56:42 2020

@author: vipul.jain
"""
def bracket(string):
   all_brackets = ['()', '{}', '[]']
   while any(x in string for x in all_brackets):
      for bracket in all_brackets:
         string = string.replace(bracket, '')
   return not string

input_string = input().strip('"')
if brackets(input_string):
   print("true")
else:
   print("false")

